import osmnx as ox, networkx as nx, pandas as pd, numpy as np
import  matplotlib.pyplot as plt
from sklearn.neighbors import KDTree
import geocoder

def pto_cerc(nodes, adr):
#Usando Ktree se busca el nodo mas cercano a un punto (lat, lng)asdasda
    tree = KDTree(nodes[['y', 'x']], metric='euclidean')
    adr_idx = tree.query([adr], k=1, return_distance=False)[0]
    aux = nodes.iloc[adr_idx].index.values[0]
    return aux

def calc_ruta (G, nodos, nodo_dstn):
    #funcion que entrega la ruta mas corta entre 3 compañias
#direcciones de las compañia de bomberos obtenidas de openstreetmap
    loc_bomb = np.array([[-39.81203, -73.24513],
                 [-39.810513, -73.255552],
                 [-39.81654, -73.23867]])

    nodo_cb = np.zeros((3,1)).astype(int)
    distancia = np.zeros((3,1)).astype(int)
    for i in range(0,3):
        #se busca el nodo mas cercano usando la función pto_cerc
        nodo_cb[i] = pto_cerc(nodos,loc_bomb[i])
        #se calcula la distancia de la ruta desde cada compañia de bombero al destino
        distancia[i] = nx.shortest_path_length(G, source=nodo_cb[i,0], target=nodo_dstn)
#nueva matriz con nodos y distancias calculadas:
    mat_aux = np.concatenate((nodo_cb,distancia),axis=1)
#ordenamos los nodos segun la menor distancia:
    mat_ordenada = np.sort(mat_aux.view('i8,i8'), order=['f1'], axis=0).view(np.int)
#Traza la ruta desde el nodo de la compañia mas cercana al destino_nodo
    ruta = nx.shortest_path(G, mat_ordenada[0,0], nodo_dstn)

    return ruta, mat_aux, mat_ordenada

def prep_map():
#se carga el mapa de valdivia desde un punto. Devuelve archivo archivo osmnx
    location_point = (-39.8153, -73.2452)
    G = ox.graph_from_point(location_point,network_type='all_private', distance=3000, simplify=False)
    nodos, _ = ox.graph_to_gdfs(G)
    nodos.head()
    return G, nodos

def map(direccion):
    #Funcion que traza la ruta mas corta entre tes posibles hacia un unico destino
    val = "Valdivia, Provincia de Valdivia, Región de Los Ríos, 5110655, Chile"
#con geocoder obtenemos los datos de latitud y longitud
    loc1 = geocoder.osm(direccion + ', ' + val)
    loc_usr = loc1.latlng  #En loc_usr se guarda una vector con los valores de (lat, lng)

    G, nodos = prep_map()            #se carga el mapa de valdivia
    destino_nodo = pto_cerc(nodos, loc_usr)
    ruta, z,x = calc_ruta(G, nodos, destino_nodo)      #entrega la ruta mas corta
    fig, ax = ox.plot_graph_route(G, ruta, fig_height=10, fig_width=10, show=False, close=False,
                              edge_color='black', orig_dest_node_color='green', route_color='green')
    print (z)
    print (x)
    print(loc_usr)

    plt.show()


def main():
    A = input("Ingrese la dirección : ")
#    A = "Panadería German, General Yañez"
    map(A)

main()
