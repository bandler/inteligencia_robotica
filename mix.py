#!/usr/bin/env python
# -*- coding: utf-8 -*-

#############################################
# Basado en:
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################

import math, cv2, argparse, time
from collections import OrderedDict
from scipy.spatial import distance as dist
#from imutils.video import VideoStream
import osmnx as ox, networkx as nx, numpy as np, pandas as pd
import  matplotlib.pyplot as plt
from sklearn.neighbors import KDTree
import geocoder, folium
import subprocess


# Variables globales
nextObjectID = 0
objects = OrderedDict()
disappeared = OrderedDict()
maxDisappeared=5


# Matrices globales
matriz_cb = np.zeros((10000,4))
matriz_cd = np.zeros((10000,2))
matriz_dir = np.zeros((10000,1))
matriz_cuenta_A = np.zeros((10000,4))
matriz_cuenta_B = np.zeros((10000,4))
matriz_cuenta_C = np.zeros((10000,4))
classes = None
with open('yolov3.txt', 'r') as f:
    classes = [line.strip() for line in f.readlines()] #Obtiene la lista ce clases

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
net = cv2.dnn.readNet('yolov3.weights', 'yolov3.cfg')

########################################################################################################
#                               Funciones
########################################################################################################
# Funcion para obtener capas de salida
def get_output_layers(net):
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    return output_layers

# Funcion para dibujar limites del objeto detectado
def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h):
    label = str(classes[class_id])
    color = COLORS[class_id]
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

def obtenerCentroide(indices,center,class_ids,boxes,confidencia,image):
    centroide=[]
    clasesIds=[]
    box=[]
    arregloCondifencia=[]
    for (i,j) in zip(indices,range(0,len(indices))):
        i = i[0]
        arregloCondifencia.append(confidencia[i])
        centroide.append(center[i])
        clasesIds.append(class_ids[i])
        box.append(boxes[i])
        x = boxes[i][0]
        y = boxes[i][1]
        w = boxes[i][2]
        h = boxes[i][3]
        draw_prediction(image, class_ids[i], confidencia[i], round(x), round(y), round(x+w), round(y+h))
    return centroide,clasesIds,box,arregloCondifencia

def actualizar(rects,disappeared,maxDisappeared,objects,nextObjectID,clasesIds,centroide):
    if len(rects) == 0:
        # Recorre cualquier objeto rastreado existente y lo marca como desaparecido
        for objectID in disappeared.keys():
            disappeared[objectID] += 1

            if disappeared[objectID] > maxDisappeared:
                del objects[objectID]
                del disappeared[objectID]
        #return objects,disappeared,nextObjectID

    # Crea una matriz de centroides de entrada para el cuadro actual.
    centroidesActuales=np.array(centroide,dtype="int")
    if len(objects) == 0:
        for i in range(0,len(centroidesActuales)):
            objects[nextObjectID] = np.concatenate((centroidesActuales[i],[clasesIds[i]]),axis=0)
            disappeared[nextObjectID] = 0
            nextObjectID += 1
    # De lo contrario, se están rastreando objetos, por lo que debemos intentar hacer coincidir los centroides de entrada con los centroides de objeto existentes
    else:
        # Asocia el conjunto de ID de objetos y los centroides correspondientes
        objectIDs = list(objects.keys())
        objectCentroids = list(objects.values())
        # Calcula la distancia entre cada par de centroides de cada objeto y el centroide de entrada respectivamente; el objetivo es hacer coincidir un centroide de entrada con un centroide de objeto existente.
        D = dist.cdist(np.array(objectCentroids)[:,0:2], centroidesActuales)
        # Para realizar esta coincidencia debemos primero encontrar el valor más pequeño en cada fila y luego ordenar los índices de las filas en función de sus valores mínimos para que la fila con el valor más pequeño se encuentre en el mismo índice de la lista
        rows = D.min(axis=1).argsort()
        # A continuación, realizamos un proceso similar en las columnas encontrando el valor más pequeño en cada columna y luego ordenándolos usando la lista de índices de filas calculada previamente.
        cols = D.argmin(axis=1)[rows]
        # Para determinar si necesitamos actualizar, registrar o eliminar un objeto, necesitamos hacer un seguimiento de cuáles de las filas y los índices de columna ya hemos examinado.
        usedRows = set()
        usedCols = set()
        # Bucle sobre la combinación de los vectores de índice (fila, columna)
        for (row, col) in zip(rows, cols):
            # Si ya hemos examinado el valor de la fila o columna antes, se ignora
            if row in usedRows or col in usedCols:
                continue
            # Si no, se toma el ID del objeto para la fila actual, se establece su nuevo centroide y se reinicia el contador de desaparecidos
            objectID = objectIDs[row]
            objects[objectID] = np.concatenate((centroidesActuales[col],[clasesIds[col]]),axis=0)
            disappeared[objectID] = 0
            # Indica que hemos examinado cada uno de los índices de fila y columna, respectivamente.
            usedRows.add(row)
            usedCols.add(col)
        #  Calcula tanto el índice de fila como el de columna que aún NO hemos examinado
        unusedRows = set(range(0, D.shape[0])).difference(usedRows)
        unusedCols = set(range(0, D.shape[1])).difference(usedCols)
        # En caso de que el número de centroides de objeto sea igual o mayor que el número de centroides de entrada que necesitamos verificar, además, ve si algunos de estos objetos han desaparecido
        if D.shape[0] >= D.shape[1]:
            # Ciclo sobre los índices de fila no utilizados
            for row in unusedRows:
                # Agarra el ID del objeto para el índice de fila correspondiente e incrementa al contador desaparecido
                objectID = objectIDs[row]
                disappeared[objectID] += 1
                # Verifica si el número de cuadros consecutivos que el objeto ha sido marcado como "desaparecido", para autorizaciones para eliminar el objeto de los registros
                if disappeared[objectID] > maxDisappeared:
                    # Elimina el objeto de los registros
                    del objects[objectID]
                    del disappeared[objectID]
        # Si no, si el número de centroides de entrada es mayor que el número de centroides de objeto existentes, necesitamos registrar cada nuevo centroide de entrada como un objeto rastreable
        else:
            for col in unusedCols:
                objects[nextObjectID] = np.concatenate((centroidesActuales[col],[clasesIds[col]]),axis=0)
                disappeared[nextObjectID] = 0
                nextObjectID += 1
    return objects,disappeared,nextObjectID

##########################################################################################################
##########################################################################################################

def medflux (vid):
    time.sleep(2.0)
    total_norte=0
    total_norte=0
    total_sur=0
    total_detenidos=0

    for q in range(10):
        # Lee el siguiente fotograma de la secuencia de video y cambia su tamaño
        (grabbed, image) = vid.read()
        #cv2.imshow("Video", image)

        # Si hemos llegado al final del vídeo salimos
        if not grabbed:
            break
        Width = image.shape[1]
        Height = image.shape[0]
        scale = 0.00392
        #classes = None



        #COLORS = np.random.uniform(0, 255, size=(len(classes), 3)) #
        blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
        net.setInput(blob)
        outs = net.forward(get_output_layers(net))
        class_ids = []
        center=[]
        confidences = []
        boxes = []
        conf_threshold = 0.5
        nms_threshold = 0.4

        # Analiza cada capa de salida
        # Obtiene la confianza de cada una y eliminas las mas debiles
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])
                    center.append([center_x,center_y])
        global_class = np.zeros((len(boxes),1))
        # Elimina recuadros repetidos (Supresion maxima)
        indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
        # Repasa los recuadros restantes y dibuja

        centroide,clasesIds,rects,confidences=obtenerCentroide(indices,center,class_ids,boxes,confidences,image)

        # Verifica si la lista de rectángulos del cuadro delimitador de entrada está vacía

        objects1,disappeared1,nextObjectID1=actualizar(rects,disappeared,maxDisappeared,objects,nextObjectID,clasesIds,centroide)
        # Devuelve el conjunto de objetos rastreables.
        # return objects
        matriz_ca = np.zeros((int(max(indices))+1,4))
        global_id = np.zeros((int(max(indices))+1,1))

        for (objectID, objeto),i,n in zip(objects1.items(),range(0,len(centroide)),range(0,len(rects))):
        # Dibuja tanto el ID del objeto como el centroide del objeto en el marco de salida, y realiza el metodo de cuenta de vehículos por clase y asigna direcciones a estos
            #Variables para direcciones de distintos vehiculos
            autos_norte=0
            autos_sur=0
            autos_detenidos=0
            motos_norte=0
            motos_sur=0
            motos_detenidos=0
            buses_norte=0
            buses_sur=0
            buses_detenidos=0
            camiones_norte=0
            camiones_sur=0
            camiones_detenidos=0
            centroid=objeto[0:2]

            global_id[i] = objectID
            global_class[i]=objeto[2]

            text = "ID {}".format(objectID)
            cv2.putText(image, text, (centroid[0] - 10, centroid[1] - 10),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv2.circle(image, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

            matriz_ca = matriz_cb
            matriz_ca[objectID,2] = matriz_ca[objectID,0]
            matriz_ca[objectID,3] = matriz_ca[objectID,1]
            matriz_ca[objectID,0] = centroid[0]
            matriz_ca[objectID,1] = centroid[1]
            matriz_cd[objectID,0] = matriz_ca[objectID,0] - matriz_ca[objectID,2]
            matriz_cd[objectID,1] = matriz_ca[objectID,1] - matriz_ca[objectID,3]

            # ASIGNA DIRECCION DE VEHICULO
            if ((matriz_cd[objectID,0] == 0) & (matriz_cd[objectID,1] == 0)):
                matriz_dir[objectID] = 3
            else:
                if (matriz_cd[objectID,1]<0):
                    matriz_dir[objectID] = 1
                else:
                    matriz_dir[objectID] = 2

            # CUENTA
            ##############################AUTO
            if objeto[2] == 2:
                if (matriz_dir[objectID]==1):
                    matriz_cuenta_A[objectID,0] = matriz_cuenta_A[objectID,0] + 0.4
                elif (matriz_dir[objectID]==2):
                    matriz_cuenta_B[objectID,0] = matriz_cuenta_B[objectID,0] + 0.4
                elif (matriz_dir[objectID]==3):
                    matriz_cuenta_C[objectID,0] = matriz_cuenta_C[objectID,0] + 0.4
            ##############################MOTO
            elif objeto[2] == 3:
                if (matriz_dir[objectID]==1):
                    matriz_cuenta_A[objectID,1] = matriz_cuenta_A[objectID,1] + 0.4
                elif (matriz_dir[objectID]==2):
                    matriz_cuenta_B[objectID,1] = matriz_cuenta_B[objectID,1] + 0.4
                elif (matriz_dir[objectID]==3):
                    matriz_cuenta_C[objectID,1] = matriz_cuenta_C[objectID,1] + 0.4
            ##############################BUS
            elif objeto[2] == 5:
                if (matriz_dir[objectID]==1):
                    matriz_cuenta_A[objectID,2] = matriz_cuenta_A[objectID,2] + 0.4
                elif (matriz_dir[objectID]==2):
                    matriz_cuenta_B[objectID,2] = matriz_cuenta_B[objectID,2] + 0.4
                elif (matriz_dir[objectID]==3):
                    matriz_cuenta_C[objectID,2] = matriz_cuenta_C[objectID,2] + 0.4
            ##############################CAMIONETA/CAMION
            elif objeto[2] == 7:
                if (matriz_dir[objectID]==1):
                    matriz_cuenta_A[objectID,3] = matriz_cuenta_A[objectID,3] + 0.4
                elif (matriz_dir[objectID]==2):
                    matriz_cuenta_B[objectID,3] = matriz_cuenta_B[objectID,3] + 0.4
                elif (matriz_dir[objectID]==3):
                    matriz_cuenta_C[objectID,3] = matriz_cuenta_C[objectID,3] + 0.4

            # Resultados flujo
            wauto = 1
            wmoto = 0.5
            wbus = 2
            wcam = 1.5
            ####### AL NORTE
            a=0
            for a in range(10000):
                if (matriz_cuenta_A[a,0] > 1):
                    autos_norte = autos_norte + 1
                if (matriz_cuenta_A[a,1] > 1):
                    motos_norte = motos_norte + 1
                if (matriz_cuenta_A[a,2] > 1):
                    buses_norte = buses_norte + 1
                if (matriz_cuenta_A[a,3] > 1):
                    camiones_norte = camiones_norte + 1
                total_norte = autos_norte + motos_norte + buses_norte + camiones_norte
                wt_norte = wauto*autos_norte + wmoto*motos_norte + wbus*buses_norte + wcam*camiones_norte
            ####### AL SUR
            a=0
            for a in range(10000):
                if (matriz_cuenta_B[a,0] > 1):
                    autos_sur = autos_sur + 1
                if (matriz_cuenta_B[a,1] > 1):
                    motos_sur = motos_sur + 1
                if (matriz_cuenta_B[a,2] > 1):
                    buses_sur = buses_sur + 1
                if (matriz_cuenta_B[a,3] > 1):
                    camiones_sur = camiones_sur + 1
                total_sur = autos_sur + motos_sur + buses_sur + camiones_sur
                wt_sur = wauto*autos_sur + wmoto*motos_sur + wbus*buses_sur + wcam*camiones_sur

            ####### DETENIDOS
            a=0
            for a in range(10000):
                if (matriz_cuenta_C[a,0] > 1):
                    autos_detenidos = autos_detenidos + 1
                if (matriz_cuenta_C[a,1] > 1):
                    motos_detenidos = motos_detenidos + 1
                if (matriz_cuenta_C[a,2] > 1):
                    buses_detenidos = buses_detenidos + 1
                if (matriz_cuenta_C[a,3] > 1):
                    camiones_detenidos = camiones_detenidos + 1
                total_detenidos = autos_detenidos + motos_detenidos + buses_detenidos + camiones_detenidos
                wt_det= wauto*autos_detenidos + wmoto*motos_detenidos + wbus*buses_detenidos + wcam*camiones_detenidos
        cv2.imshow("Video", image)
        #time.sleep(1)
    print("-------------------------------------------------------------------------------")
    print("Total vehiculos al norte: ",total_norte)
    print("Total vehiculos al sur: ",total_sur)
    print("Total vehiculos detenidos: ",total_detenidos)
    print("-------------------------------------------------------------------------------")

    return wt_norte

#########################################################################################
###########################################################################################
def pto_cerc(nodes, adr):
# Usando Ktree se busca el nodo mas cercano a un punto (lat, lng)asdasda
    tree = KDTree(nodes[['y', 'x']], metric='euclidean')
    adr_idx = tree.query([adr], k=1, return_distance=False)[0]
    aux = nodes.iloc[adr_idx].index.values[0]
    return aux

def cal_flux():
# Funcion que analiza 3 videos y devuelve un valor correspondiente al flujo vehicular
    video1 = cv2.VideoCapture("sim-1.mp4")
    video2 = cv2.VideoCapture("sim-2.mp4")
    video3 = cv2.VideoCapture("sim-3.mp4")

    mat_video = [video1, video2,video3]
    flujo = np.zeros(3).astype('int')

    for ñ in range(3):
        flujo[ñ] = medflux(mat_video[ñ])

    return flujo

def calc_ruta (G, nodos, nodo_dstn):
    # Funcion que entrega la ruta mas corta entre 3 compañias
    # direcciones de las compañia de bomberos obtenidas de openstreetmap
    loc_bomb = np.array([[-39.8322562, -73.2168618],
                 [-39.810513, -73.255552],
                 [-39.81654, -73.23867]])
    nodo_cb = np.zeros((3,1)).astype(int)
    distancia = np.zeros((3,1)).astype(int)
    for i in range(0,3):
        # Se busca el nodo mas cercano usando la función pto_cerc
        nodo_cb[i] = pto_cerc(nodos,loc_bomb[i])
        # Se calcula la distancia de la ruta desde cada compañia de bombero al destino
        distancia[i] = nx.shortest_path_length(G, source=nodo_cb[i,0], target=nodo_dstn)
# Nueva matriz con nodos y distancias calculadas:
    mat_aux = np.concatenate((nodo_cb,distancia),axis=1) #######################  <------------- modificar
# Se inicia el analisis de video para obtener el peso del flujo vehicular
    flujo = cal_flux()     # vector de la forma (flujoVideo-1, flujoVideo-2,flujoVideo-3)
    # A la distancia calculada le agregamos el peso del flujo vehicular:
    for i in range(3):
        mat_aux[i,1]=mat_aux[i,1]+flujo[i]
    # Ordenamos por distancia + peso del flujo:
    mat_ordenada = np.sort(mat_aux.view('i8,i8'), order=['f1'], axis=0).view(np.int)
# Traza la ruta desde el nodo de la compañia mas cercana al destino_nodo
    ruta = nx.shortest_path(G, mat_ordenada[0,0], nodo_dstn)
    ruta2 = nx.shortest_path(G, mat_ordenada[1,0], nodo_dstn)
    ruta3 = nx.shortest_path(G, mat_ordenada[2,0], nodo_dstn)

    return ruta, ruta2, ruta3, loc_bomb


def prep_map():
# Se carga el mapa de valdivia desde un punto. Devuelve archivo archivo osmnx
    location_point = (-39.8153, -73.2452)
    G = ox.graph_from_point(location_point,network_type='all_private', distance=3000, simplify=False)
    nodos, _ = ox.graph_to_gdfs(G)
    nodos.head()
    return G, nodos

def map(direccion):
    # Funcion que traza la ruta mas corta entre tes posibles hacia un unico destino
    G, nodos = prep_map()            #se carga el mapa de valdivia
    val = "Valdivia, Provincia de Valdivia, Región de Los Ríos, 5110655, Chile"
# Con geocoder obtenemos los datos de latitud y longitud
    loc1 = geocoder.osm(direccion + ', ' + val)
    loc_usr = loc1.latlng  #En loc_usr se guarda una vector con los valores de (lat, lng)

    destino_nodo = pto_cerc(nodos, loc_usr)

    ruta, ruta1, ruta2, loc_bomb = calc_ruta(G, nodos, destino_nodo)

    route_map = ox.plot_route_folium(G, ruta, route_color='green')
    ox.plot_route_folium(G, ruta1,route_map=route_map, route_color='red')
    ox.plot_route_folium(G, ruta2,route_map=route_map, route_color='red')

    folium.Marker(location=loc_usr, icon=folium.Icon(color='red')).add_to(route_map)
    folium.Marker(location=loc_bomb[0], icon=folium.Icon(color='blue')).add_to(route_map)
    folium.Marker(location=loc_bomb[1], icon=folium.Icon(color='blue')).add_to(route_map)
    folium.Marker(location=loc_bomb[2], icon=folium.Icon(color='blue')).add_to(route_map)

    filepath = 'route.html'
    route_map.save(filepath)


def main():
    A = input("Ingrese la dirección : ")

    map(A)
    subprocess.Popen(['firefox','route.html','-new-tab'])


main()
