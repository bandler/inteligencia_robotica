#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2
import time
 
# Cargamos el vídeo
#camara = cv2.VideoCapture("detector-movimiento-opencv.mp4")
camara = cv2.VideoCapture("out.ogv")
# Inicializamos el primer frame a vacío.^
# Nos servirá para obtener el fondo
fondo = None
 
# Recorremos todos los frames
while True:
	# Obtenemos el frame
	(grabbed, frame) = camara.read()
 
	# Si hemos llegado al final del vídeo salimos
	if not grabbed:
		break
 
	# Convertimos a escala de grises
	#ret,frame2=camara.read()
	gris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
 	#gris2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
	# Aplicamos suavizado para eliminar ruido
	gris = cv2.GaussianBlur(gris, (21, 21), 0)
	#fondo=cv2.GaussianBlur(gris2,(21,21),0)
	# Si todavía no hemos obtenido el fondo, lo obtenemos
	# Será el primer frame que obtengamos
	if fondo is None:
		fondo = gris
		continue
 
	# Calculo de la diferencia entre el fondo y el frame actual
	resta = cv2.absdiff(fondo, gris)
 
	# Aplicamos un umbral
	umbral = cv2.threshold(resta, 20, 255, cv2.THRESH_BINARY)[1]
 
	# Dilatamos el umbral para tapar agujeros
	umbral = cv2.dilate(umbral, None, iterations=2)
 
	# Copiamos el umbral para detectar los contornos
	areaEstudio=umbral[305:375,230:400]
	cv2.rectangle(frame,(230,305),(400,375),(0,0,255),1)
	contornosimg = areaEstudio.copy()
 
	# Buscamos contorno en la imagen
	im, contornos, hierarchy = cv2.findContours(contornosimg,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
 
	# Recorremos todos los contornos encontrados
	for c in contornos:
		# Eliminamos los contornos más pequeños
		if cv2.contourArea(c) < 500 and cv2.contourArea(c)>400:
			continue
 
		# Obtenemos el bounds del contorno, el rectángulo mayor que engloba al contorno
		(x, y, w, h) = cv2.boundingRect(c)
		# Dibujamos el rectángulo del bounds
		#cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
		cv2.rectangle(frame, (x+230, y+305), (x+230 + w, y+305 + h), (0, 255, 0), 2)
 
	# Mostramos las imágenes de la cámara, el umbral y la resta
	cv2.imshow("Camara", frame)
	cv2.imshow("Umbral", umbral)
	cv2.imshow("Resta", resta)
	cv2.imshow("Contrornos",contornosimg)
	#cv2.imshow("Contorno", im)
 
	# Capturamos una tecla para salir
	key = cv2.waitKey(1) & 0xFF
 
	# Tiempo de espera para que se vea bien
	time.sleep(0.015)
 #230 360
 #331
	# Si ha pulsado la letra s, salimos
	if key == ord("s"):
		break
 
# Liberamos la cámara y cerramos todas las ventanas
camara.release()
cv2.destroyAllWindows()