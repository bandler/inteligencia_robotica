#!/usr/bin/env python
# -*- coding: utf-8 -*-

#############################################
# Basado en:
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################

import math
import cv2
import argparse
import numpy as np
from collections import OrderedDict
from scipy.spatial import distance as dist
#from imutils.video import VideoStream
import time

# Variables globales
nextObjectID = 0
objects = OrderedDict()
disappeared = OrderedDict()
maxDisappeared=5


# Matrices globales
matriz_cb = np.zeros((10000,4))
matriz_cd = np.zeros((10000,2))
matriz_dir = np.zeros((10000,1))
matriz_cuenta_A = np.zeros((10000,4))
matriz_cuenta_B = np.zeros((10000,4))
matriz_cuenta_C = np.zeros((10000,4))
classes = None
with open('yolov3.txt', 'r') as f:
    classes = [line.strip() for line in f.readlines()] #Obtiene la lista ce clases

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
net = cv2.dnn.readNet('yolov3.weights', 'yolov3.cfg')
########################################################################################################
########################################################################################################
#                               Funciones
########################################################################################################

# Funcion para obtener capas de salida
def get_output_layers(net):
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    return output_layers

# funcion para dibujar limites del objeto detectado
def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h):
    label = str(classes[class_id])
    color = COLORS[class_id]
    #print(x)
    #print(y)
    #print(x_plus_w)
    #print(y_plus_h)
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    #return label

def obtenerCentroide(indices,center,class_ids,boxes,confidencia):
    centroide=[]
    clasesIds=[]
    box=[]
    arregloCondifencia=[]
    for (i,j) in zip(indices,range(0,len(indices))):
        i = i[0]
        #boxx = boxes[i]
        arregloCondifencia.append(confidencia[i])
        centroide.append(center[i])
        clasesIds.append(class_ids[i])
        box.append(boxes[i])
        x = boxes[i][0]
        y = boxes[i][1]
        w = boxes[i][2]
        h = boxes[i][3]
        draw_prediction(image, class_ids[i], confidencia[i], round(x), round(y), round(x+w), round(y+h))
        #global_class[i] = class_ids[i]
    return centroide,clasesIds,box,arregloCondifencia

def actualizar(rects,disappeared,maxDisappeared,objects,nextObjectID,clasesIds):
    if len(rects) == 0:
        # loop over any existing tracked objects and mark them
        # as disappeared
        for objectID in disappeared.keys():
            disappeared[objectID] += 1

            if disappeared[objectID] > maxDisappeared:
                del objects[objectID]
                del disappeared[objectID]
        return objects,disappeared,nextObjectID

    # initialize an array of input centroids for the current frame
    #inputCentroids = np.zeros((len(rects), 2), dtype="int")
    centroidesActuales=np.array(centroide,dtype="int")
    if len(objects) == 0:
        for i in range(0,len(centroidesActuales)):
            #register(inputCentroids[i])
            objects[nextObjectID] = np.concatenate((centroidesActuales[i],[clasesIds[i]]),axis=0)
            disappeared[nextObjectID] = 0
            nextObjectID += 1
    # otherwise, are are currently tracking objects so we need to
    # try to match the input centroids to existing object
    # centroids
    else:
        # grab the set of object IDs and corresponding centroids
        objectIDs = list(objects.keys())
        objectCentroids = list(objects.values())
        # compute the distance between each pair of object
        # centroids and input centroids, respectively -- our
        # goal will be to match an input centroid to an existing
        # object centroid
        D = dist.cdist(np.array(objectCentroids)[:,0:2], centroidesActuales)
        # in order to perform this matching we must (1) find the
        # smallest value in each row and then (2) sort the row
        # indexes based on their minimum values so that the row
        # with the smallest value as at the *front* of the index
        # list
        rows = D.min(axis=1).argsort()
        # next, we perform a similar process on the columns by
        # finding the smallest value in each column and then
        # sorting using the previously computed row index list
        cols = D.argmin(axis=1)[rows]
        # in order to determine if we need to update, register,
        # or deregister an object we need to keep track of which
        # of the rows and column indexes we have already examined
        usedRows = set()
        usedCols = set()
        # loop over the combination of the (row, column) index
        # tuples
        for (row, col) in zip(rows, cols):
            # if we have already examined either the row or
            # column value before, ignore it
            # val
            if row in usedRows or col in usedCols:
                continue
            # otherwise, grab the object ID for the current row,
            # set its new centroid, and reset the disappeared
            # counter
            objectID = objectIDs[row]
            objects[objectID] = np.concatenate((centroidesActuales[col],[clasesIds[col]]),axis=0)
            disappeared[objectID] = 0
            # indicate that we have examined each of the row and
            # column indexes, respectively
            usedRows.add(row)
            usedCols.add(col)
        # compute both the row and column index we have NOT yet
        # examined
        unusedRows = set(range(0, D.shape[0])).difference(usedRows)
        unusedCols = set(range(0, D.shape[1])).difference(usedCols)
        # in the event that the number of object centroids is
        # equal or greater than the number of input centroids
        # we need to check and see if some of these objects have
        # potentially disappeared
        if D.shape[0] >= D.shape[1]:
            # loop over the unused row indexes
            for row in unusedRows:
                # grab the object ID for the corresponding row
                # index and increment the disappeared counter
                objectID = objectIDs[row]
                disappeared[objectID] += 1
                # check to see if the number of consecutive
                # frames the object has been marked "disappeared"
                # for warrants deregistering the object
                if disappeared[objectID] > maxDisappeared:
                    #deregister(objectID)
                    del objects[objectID]
                    del disappeared[objectID]
        # otherwise, if the number of input centroids is greater
        # than the number of existing object centroids we need to
        # register each new input centroid as a trackable object
        else:
            for col in unusedCols:
                objects[nextObjectID] = np.concatenate((centroidesActuales[col],[clasesIds[col]]),axis=0)
                disappeared[nextObjectID] = 0
                nextObjectID += 1
    return objects,disappeared,nextObjectID
##########################################################################################################
##########################################################################################################
vid =cv2.VideoCapture("sim-1.mp4")
time.sleep(2.0)

while True:
    # read the next frame from the video stream and resize it
    (grabbed, image) = vid.read()

    # Si hemos llegado al final del vídeo salimos
    if not grabbed:
        break
    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392
    #classes = None



    #COLORS = np.random.uniform(0, 255, size=(len(classes), 3)) #
    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))
    class_ids = []
    center=[]
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    # Analiza cada capa de salida
    # Obtiene la confianza de cada una y eliminas las mas debiles
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])
                center.append([center_x,center_y])
    global_class = np.zeros((len(boxes),1))
    # Elimina recuadros repetidos (Supresion maxima)
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    # Repasa los recuadros restantes y dibuja

    centroide,clasesIds,rects,confidences=obtenerCentroide(indices,center,class_ids,boxes,confidences)
    #draw_prediction(img, class_id, confidences, x, y, x_plus_w, y_plus_h):

    # check to see if the list of input bounding box rectangles
    # is empty

    objects,disappeared,nextObjectID=actualizar(rects,disappeared,maxDisappeared,objects,nextObjectID,clasesIds)
    # return the set of trackable objects
    # return objects
    matriz_ca = np.zeros((int(max(indices))+1,4))
    global_id = np.zeros((int(max(indices))+1,1))

    for (objectID, objeto),i,n in zip(objects.items(),range(0,len(centroide)),range(0,len(rects))):
    # draw both the ID of the object and the centroid of the
    # object on the output frame
        #Variables para direcciones de distintos vehiculos
        autos_norte=0
        autos_sur=0
        autos_detenidos=0
        motos_norte=0
        motos_sur=0
        motos_detenidos=0
        buses_norte=0
        buses_sur=0
        buses_detenidos=0
        camiones_norte=0
        camiones_sur=0
        camiones_detenidos=0
        total_norte=0
        total_sur=0
        total_detenidos=0
        centroid=objeto[0:2]

        global_id[i] = objectID
        global_class[i]=objeto[2]

        text = "ID {}".format(objectID)
        cv2.putText(image, text, (centroid[0] - 10, centroid[1] - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.circle(image, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

        #print("Class: ",int(global_class[n]), "ID: ", int(global_id[n]))
        #i = i[0]
        matriz_ca = matriz_cb
        matriz_ca[objectID,2] = matriz_ca[objectID,0]
        matriz_ca[objectID,3] = matriz_ca[objectID,1]
        matriz_ca[objectID,0] = centroid[0]
        matriz_ca[objectID,1] = centroid[1]
        matriz_cd[objectID,0] = matriz_ca[objectID,0] - matriz_ca[objectID,2]
        matriz_cd[objectID,1] = matriz_ca[objectID,1] - matriz_ca[objectID,3]

        # ASIGNA DIRECCION DE VEHICULO
        if ((matriz_cd[objectID,0] == 0) & (matriz_cd[objectID,1] == 0)):
            matriz_dir[objectID] = 3
            print("El ID ",(objectID)," esta detenido.")
        else:
            if (matriz_cd[objectID,1]<0):
                matriz_dir[objectID] = 1
                print("El ID ",(objectID)," va hacia \"arriba\".")
            else:
                matriz_dir[objectID] = 2
                print("El ID ",(objectID)," va hacia \"abajo\".")
        # CUENTA
        ##############################AUTO
        if objeto[2] == 2:
            if (matriz_dir[objectID]==1):
                matriz_cuenta_A[objectID,0] = matriz_cuenta_A[objectID,0] + 0.4
            elif (matriz_dir[objectID]==2):
                matriz_cuenta_B[objectID,0] = matriz_cuenta_B[objectID,0] + 0.4
            elif (matriz_dir[objectID]==3):
                matriz_cuenta_C[objectID,0] = matriz_cuenta_C[objectID,0] + 0.4
        ##############################MOTO
        elif objeto[2] == 3:
            if (matriz_dir[objectID]==1):
                matriz_cuenta_A[objectID,1] = matriz_cuenta_A[objectID,1] + 0.4
            elif (matriz_dir[objectID]==2):
                matriz_cuenta_B[objectID,1] = matriz_cuenta_B[objectID,1] + 0.4
            elif (matriz_dir[objectID]==3):
                matriz_cuenta_C[objectID,1] = matriz_cuenta_C[objectID,1] + 0.4
        ##############################BUS
        elif objeto[2] == 5:
            if (matriz_dir[objectID]==1):
                matriz_cuenta_A[objectID,2] = matriz_cuenta_A[objectID,2] + 0.4
            elif (matriz_dir[objectID]==2):
                matriz_cuenta_B[objectID,2] = matriz_cuenta_B[objectID,2] + 0.4
            elif (matriz_dir[objectID]==3):
                matriz_cuenta_C[objectID,2] = matriz_cuenta_C[objectID,2] + 0.4
        ##############################CAMIONETA/CAMION
        elif objeto[2] == 7:
            if (matriz_dir[objectID]==1):
                matriz_cuenta_A[objectID,3] = matriz_cuenta_A[objectID,3] + 0.4
            elif (matriz_dir[objectID]==2):
                matriz_cuenta_B[objectID,3] = matriz_cuenta_B[objectID,3] + 0.4
            elif (matriz_dir[objectID]==3):
                matriz_cuenta_C[objectID,3] = matriz_cuenta_C[objectID,3] + 0.4

        # Resultados flujo
        ####### AL NORTE
        a=0
        for a in range(10000):
            if (matriz_cuenta_A[a,0] > 1):
                autos_norte = autos_norte + 1
            if (matriz_cuenta_A[a,1] > 1):
                motos_norte = motos_norte + 1
            if (matriz_cuenta_A[a,2] > 1):
                buses_norte = buses_norte + 1
            if (matriz_cuenta_A[a,3] > 1):
                camiones_norte = camiones_norte + 1
            total_norte = autos_norte + motos_norte + buses_norte + camiones_norte

        ####### AL SUR
        a=0
        for a in range(10000):
            if (matriz_cuenta_B[a,0] > 1):
                autos_sur = autos_sur + 1
            if (matriz_cuenta_B[a,1] > 1):
                motos_sur = motos_sur + 1
            if (matriz_cuenta_B[a,2] > 1):
                buses_sur = buses_sur + 1
            if (matriz_cuenta_B[a,3] > 1):
                camiones_sur = camiones_sur + 1
            total_sur = autos_sur + motos_sur + buses_sur + camiones_sur

        ####### DETENIDOS
        a=0
        for a in range(10000):
            if (matriz_cuenta_C[a,0] > 1):
                autos_detenidos = autos_detenidos + 1
            if (matriz_cuenta_C[a,1] > 1):
                motos_detenidos = motos_detenidos + 1
            if (matriz_cuenta_C[a,2] > 1):
                buses_detenidos = buses_detenidos + 1
            if (matriz_cuenta_C[a,3] > 1):
                camiones_detenidos = camiones_detenidos + 1
            total_detenidos = autos_detenidos + motos_detenidos + buses_detenidos + camiones_detenidos


#    print("-------------------------------------------------------------------------------")
#    print("Matriz distancias: ")
#    print(matriz_ca)
#    matriz_cb = matriz_ca
#    print("-------------------------------------------------------------------------------")
#    print("Distancias: ")
#    print(matriz_cd)
    print("-------------------------------------------------------------------------------")
    print("Total vehiculos al norte: ",total_norte)
    print("Total vehiculos al sur: ",total_sur)
    print("Total vehiculos detenidos: ",total_detenidos)
    print("-------------------------------------------------------------------------------")
    cv2.imshow("Video", image)
    key = cv2.waitKey(1) & 0xFF
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break
#cv2.imwrite("object-detection.jpg", image)
cv2.destroyAllWindows()
