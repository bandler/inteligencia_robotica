## Nombre del Proyecto
Sistema de creación de ruta óptima para carros de bombero.

## Pre-requisitos
Listado de librerias previas a instalar

* osmnx (Basada en Open Street Map, permite análisis de y visualización de mapas)

* networkx (Estudio de gráficos y redes)

* numpy (Agrega soporte para trabajar con vectores y matrices)

* matplotlib.pyplot (Generación de gráficos a partir de datos contenidos en listas o arrays)

* geocoder (Trabajar con servicios de localización)

* KDTree de sklearn.neighbors (Busca las rutas más cortas a través de un análisis de redes de nodos)

* folium (Transforma las rutas de python a html para verlo en un navegador web)

* cv2 (Librería de visión artificial para el procesamiento de imágenes)

* scipy (Librería de funciones matemáticas complejas, usada para el módulo de optimización)
---
## Autores
* Brian Andler
* Angelo Guzman
* Christian Santibañez
* Fabian Olmos
* Camilo Sepulveda
* Manuel Ojeda
---
## Objetivos
ESTE PROYECTO TIENE COMO FIN MOSTRAR LA UTILIZACIÓN DE HERRAMIENTAS DE INTELIGENCIA ROBÓTICA PARA ENCONTRAR UNA RUTA ÓPTIMA PARA UN CARRO DE BOMBEROS QUE SE DIRIGUE HACIA UN FOCO DE INCENDIOS.

	**Objetivo Principal**

* Encontrar la ruta mas corta y con menor flujo vehicular desde tres compañias de bomberos distintas hacia un foco de incendio mediante herramientas de inteligencia robótica

	**Objetivos secundarios**

* Determinar el flujo vehicular mediante la identificación de distintos medios de transporte en un video de las calles de las posibles rutas

* Medir distancia de las trayectorias posibles que podrian tomar los carros de bomberos

---
##Descripción del código

 * Al ejecutar mix.py se pedirá al usuario ingresar una dirección que pertenezca a la ciudad de Valdivia.
 Se mostrará en pantalla la cantidad de vehiculos que van en dirección norte, sur y que estén detenidos en tres puntos diferentes. Luego de eso, se abrirá el navegador web indicando con un marcador azul la posición de 3 compañias de bomberos, en un marcador rojo la dirección ingresada. Junto con ello se muestran las tres rutas más cortas calculadas desde cada compañía de bomberos hasta el destino ingresado, mostrando en color verde la ruta con menos tráfico vehicular.

##Funcionamiento del código

* **mix.py** inicia desde la función **main()** que pide ingresar la dirección de destino, la cual es ingresada en la función **map()**.
* **map** recoge la dirección ingresada y la traduce en un punto espacial de latitud y longitud. En esta función tambien se llama **prep_map()**, la cual descarga el mapa de Valdivia desde openstreetmap y los archivos de geodataframe que se necesitarán en otras instancias. Otra función presente en *map* es **pto_cerc()** que devuelve el nodo mas cercano a la dirección ingresada. Obtenido los nodos se inicia la función **calc_ruta()** para que entregue las ruta optima desde tres compañias de bomberos.
* **pto_cerc** utiliza el algortimo génetico KDTree para encontrar el nodo más cercano, dado los valores de latitud y longitud. Esto es necesario, ya que el archivo del geodataframe contiene una muy alta cantidad de nodos que identifican a una cooredenada (entre otros datos disponibles).
Con la rutas obtenidas se crea un archivo *HTML* ("route.html") para una mejor visalización del mapa obtenido y las rutas
* **calc_ruta** contiene una matriz llamada *loc_bomb* que guarda las coordenadas geográficas de tres puntos que corresponden a compañias de bomberos en Valdivia.
El cálculo de la ruta optima la realiza la libreria de networkx con la función *shortest_path()* el cual usa como algoritmo de optimización, el algoritmo de Dijkstra.
En esta función tambien se obtiene la ruta con menor flujo vehicular. Para ello se ejecuta la funcion **cal_flux()**
* **cal_flux** carga tres videos, que simularán el tráfico vehicular en algun punto de cada una de las tres rutas que se analizan. Para contar cuántos vehiculos hay en escena se llama a la función **medflux()** que realiza el analisis de imágen, identifica y clasifica autos, motos, camiones y buses que van en dirección norte (a favor del tráfico), en direccion sur (dirección contraria) y la cantidad de autos DETENIDOS; y devuelve la cantidad que hay de cada uno de ellos.
* **medflux** carga un video, luego clasifica vehículos según su tipo (moto, automóvil,etc), mediante una red neuronal entrenada con yolo, además a cada vehículo se le asigna una ID. Después se carga la función **obtenerCentroide** la cual devuelve el centroide de la figura que se generó a partir de los objetos reconocidos por **medflux**. Posteriormente se carga la función **actualizar**. Finalmente, cuenta la cantidad de vehículos, discriminando por su tipo y dirección; además, dependiendeo del tipo de vehículo se le asigna un peso (mayor según el tamaño del vehículo).
* **actualizar** permite hacer el siguimiento de las IDs y agregar nuevas IDs a vehículos que entren en escena.
